<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shopping extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'created_date'
    ];
}
